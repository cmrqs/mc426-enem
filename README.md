# MC426 - ENEM

**Universidade Estadual de Campinas**
2º semestre de 2020

## Grupo
* 213037 - André Luis Romão Gouvêa
* 214327 - Carlos Eduardo Pereira Marques
* 217295 - Guilherme Ramirez
* 206341 - Tiago Pereira Dall'oca

## Resumo
A ideia é implementar um sistema baseado em dados abertos, no caso, o Enem. A partir de um website, será desenvolvido um questionário para o usuário e, com base em suas respostas e das de candidatos de anos anteriores, será feita uma predição e apresentado um intervalo de confiança para a possível nota desse candidato. 
Além disso, também serão disponibilizadas análises de dados relevantes, tais como a correlação entre características socioeconômicas e nota, a relação entre as perguntas do questionário entre si, dentre outras.

## Video

Uma apresentação do projeto está disponível a partir desse link: https://youtu.be/cobnw3q2g0s

## Build & Deploy

O **build** é feito através do script `setup-build`. Esse script irá instalar dependências, baixar os microdados, compilar o modelo, etc.

O **deploy** então pode ser feito automaticamente ao versionar o modelo gerado e executar o `git push` para esse repositório e será feito assim como descrito no arquivo `.gitlab-ci`. O deploy só se encarrega de compilar o modelo gerado para o formato `json` que será carregado no site através do `tensorflow.js`. Não daria para treinar uma rede neural como *CI* porque é muito demorado.

## Dados
Nesse repositório há um *script* de instalação, em Python, responsável por baixar e normalizar o *dataset* que será utilizado para a visualização dos dados e treinamento do modelo de predição. Tal dataset refere-se aos microdados do Enem 2019, o mais recente. Para mais informações, acesse: http://inep.gov.br/microdados.
Para efetuar essa instalação inicial, é necessário possuir o [Python 3](https://www.python.org/downloads/) instalado e executar o arquivo `/enem-analysis/setup`. O resultado dessa operação será salvo em `/data/normalized/microdados_enem_2019.csv`.

## Deploy
O projeto será feito em HTML e hospedado pelo próprio Gitlab Pages a partir do endereço: https://carl.marqs.gitlab.io/mc426-enem/. O *deploy* será feito automaticamente através de uma *pipeline* configurada neste repositório, que será disparada automaticamente a qualquer *commit* na branch `master`. Os arquivos do website estão contidos na pasta `public`.

## Configurações
No desenvolvimento do *website*, utilizaremos a biblioteca do JavaScript [jQuery](https://jquery.com/) com o *script* [Chart.js](https://www.chartjs.org/) para a construção de diferentes gráficos para a visualização do *dataset* do Enem.

## Descrição da Arquitetura

![1](doc/diagrama/1.jpeg)

![2](doc/diagrama/2.jpeg)

![3](doc/diagrama/3.jpeg)

Para o componente de backend será utilizada uma arquitetura de pipes and filters, a pipeline será composta pelas seguintes etapas:
- baixar dados
- descompactar
- normalizar os dados
- inserir em sqlite3
- filtro / query (usando sql)
- aplicação de um método estatístico baseada nos dados do questionário
- visualização (plot)

Especialmente para essa parte do projeto, iremos fazer o uso de **padrões de projeto de programação funcional**, tendo em vista a importância de valores e processamento de dados.

Neste projeto, também usaremos o estilo de arquitetura MVC (Model-View-Controller), onde teremos que o site HTML será a interface gráfica e intuitiva que será apresentada para o usuário (View); um controller intermediando e sincronizando as funções de Back End com o site visual; e a parte do código em que faremos a conexão com a tabela de dados, análise e predição de nota (Model).

Para a parte WEB, iremos fazer o uso de **padrão de projeto de _facade_**.
