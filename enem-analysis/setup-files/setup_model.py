# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython import get_ipython

# %%
import pandas as pd
import numpy as np
import sqlite3


# %%
columns_subset = [
 'sg_uf_residencia',
 'nu_idade',
 'tp_sexo',
 'tp_estado_civil',
 'tp_cor_raca',
 'tp_ano_concluiu',
 'tp_escola',
 'nu_nota_cn',
 'nu_nota_ch',
 'nu_nota_lc',
 'nu_nota_mt',
 'tp_lingua',
 'nu_nota_redacao',
 'q001',
 'q002',
 'q003',
 'q004',
 'q005',
 'q006',
 'q007',
 'q008',
 'q009',
 'q010',
 'q011',
 'q012',
 'q013',
 'q014',
 'q015',
 'q016',
 'q017',
 'q018',
 'q019',
 'q020',
 'q021',
 'q022',
 'q023',
 'q024',
 'q025',
]

columns_min_max = {
 'nu_idade': [10, 100],
 'q001': [0, 6],
 'q002': [0, 6],
 'q003': [0, 4],
 'q004': [0, 4],
 'q005': [0, 19],
 'q006': [0, 16],
 'q007': [0, 3],
 'q008': [0, 4],
 'q009': [0, 4],
 'q010': [0, 4],
 'q011': [0, 4],
 'q012': [0, 4],
 'q013': [0, 4],
 'q014': [0, 4],
 'q015': [0, 4],
 'q016': [0, 4],
 'q017': [0, 4],
 'q019': [0, 4],
 'q022': [0, 4],
 'q024': [0, 4]
}

dummies = {
    'sg_uf_residencia': range(-1,27),
    'tp_estado_civil': range(-1,4),
    'tp_cor_raca': range(-1,6),
    'tp_ano_concluiu': range(-1,12),
    'tp_escola': list(range(1, 5)) + [-1],
}


# %%
import os
script_dir = os.path.dirname(__file__)
rel_path = '../data/microdados_enem_2019.db'
abs_db_path = os.path.join(script_dir, rel_path)
conn = sqlite3.connect(abs_db_path)


# %%
chunks = pd.read_sql_query('''SELECT sg_uf_residencia,nu_idade,tp_sexo,tp_estado_civil,tp_cor_raca,tp_ano_concluiu,tp_escola,nu_nota_cn,nu_nota_ch,nu_nota_lc,nu_nota_mt,tp_lingua,nu_nota_redacao,q001,q002,q003,q004,q005,q006,q007,q008,q009,q010,q011,q012,q013,q014,q015,q016,q017,q018,q019,q020,q021,q022,q023,q024,q025 FROM microdados_enem_2019''', conn, chunksize=1000)


# %%
from pandas.api.types import CategoricalDtype

def set_dummies(df, dummies):
    for column,cats in dummies.items(): 
        cat_type = CategoricalDtype(categories=cats, ordered=True)
        df[column] = df[column].astype(cat_type)
    return pd.get_dummies(df, columns=list(dummies.keys()))


# %%
def normalize_continous_vars(df, columns_min_max):
    for column, min_max in columns_min_max.items():
        mn,mx = min_max
        df[column] = df[column].apply(lambda x: (x - mn) / (mx - mn) if x >= mn and x <= mx else -1)
    return df


# %%
base_dir = "../data/normalized/parts/"
output_dir = os.path.join(script_dir, base_dir)


# %%
import os
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

rows_count = 0
parts = []

for i, c in enumerate(chunks):
    try:
        c.columns = [x.lower() for x in c.columns]
        c = c.apply(pd.to_numeric)
        part = "part{:04d}".format(i)
        rows = len(c.index)
        set_dummies(normalize_continous_vars(c, columns_min_max), dummies).to_csv(output_dir + part + ".csv", index=False, float_format="%.3f")
        rows_count += rows
        parts.append(i)
        print("{} written, {} entries added.".format(part, rows))
    except:
        print("{} not written.".format(part))

print("---")
print("Finished. Total rows count: {}".format(rows_count))


# %%
normalized_columns = 'nu_idade,tp_sexo,nu_nota_cn,nu_nota_ch,nu_nota_lc,nu_nota_mt,tp_lingua,nu_nota_redacao,q001,q002,q003,q004,q005,q006,q007,q008,q009,q010,q011,q012,q013,q014,q015,q016,q017,q018,q019,q020,q021,q022,q023,q024,q025,sg_uf_residencia_-1,sg_uf_residencia_0,sg_uf_residencia_1,sg_uf_residencia_2,sg_uf_residencia_3,sg_uf_residencia_4,sg_uf_residencia_5,sg_uf_residencia_6,sg_uf_residencia_7,sg_uf_residencia_8,sg_uf_residencia_9,sg_uf_residencia_10,sg_uf_residencia_11,sg_uf_residencia_12,sg_uf_residencia_13,sg_uf_residencia_14,sg_uf_residencia_15,sg_uf_residencia_16,sg_uf_residencia_17,sg_uf_residencia_18,sg_uf_residencia_19,sg_uf_residencia_20,sg_uf_residencia_21,sg_uf_residencia_22,sg_uf_residencia_23,sg_uf_residencia_24,sg_uf_residencia_25,sg_uf_residencia_26,tp_estado_civil_-1,tp_estado_civil_0,tp_estado_civil_1,tp_estado_civil_2,tp_estado_civil_3,tp_cor_raca_-1,tp_cor_raca_0,tp_cor_raca_1,tp_cor_raca_2,tp_cor_raca_3,tp_cor_raca_4,tp_cor_raca_5,tp_ano_concluiu_-1,tp_ano_concluiu_0,tp_ano_concluiu_1,tp_ano_concluiu_2,tp_ano_concluiu_3,tp_ano_concluiu_4,tp_ano_concluiu_5,tp_ano_concluiu_6,tp_ano_concluiu_7,tp_ano_concluiu_8,tp_ano_concluiu_9,tp_ano_concluiu_10,tp_ano_concluiu_11,tp_escola_1,tp_escola_2,tp_escola_3,tp_escola_4,tp_escola_-1'.split(',')
normalized_columns


# %%
len(normalized_columns)


# %%
train_columns_n = len(normalized_columns) - 5


# %%
from random import shuffle

test_parts = parts[0:800]
train_parts = parts[800:]

# %%
validation = train_parts[:500]
partial_train = train_parts[500:]
target_labels = ["nu_nota_mt", "nu_nota_cn", "nu_nota_ch", "nu_nota_lc", "nu_nota_redacao"]


# %%
part_format = "part{:04d}.csv"

def part_reader(parts):
    while True:
        for p in parts:
            data = pd.read_csv(
                    os.path.abspath(os.path.join(script_dir, base_dir + part_format.format(p))))
            yield (data.drop(target_labels, axis=1).values,data[target_labels].values)

def part_reader_f(parts):
    while True:
        for p in parts:
            data = pd.read_csv(
                    os.path.abspath(os.path.join(script_dir, base_dir + part_format.format(p))))
            y = data[target_labels].values.reshape((-1, 5))
            x = data.drop(target_labels, axis=1).values.reshape(-1 ,train_columns_n)
            yield (x, y)

# %%
from keras import models
from keras import layers
from keras.models import Model
from keras import regularizers

inp = layers.Input((train_columns_n,))
x = layers.Dense(128, activation='relu')(inp)
x = layers.Dense(256, activation='relu')(x)
x = layers.Dense(128, activation='relu')(x)
x = layers.Dense(64, activation='relu')(x)
x = layers.Dense(32, activation='relu')(x)
#x = layers.Dense(512, activation='relu')(x)
#x = layers.Dense(1024, activation='relu')(x)
#x = layers.Dense(2048, activation='relu')(x)
'''
x = layers.Dense(256, activation='relu')(x)
x = layers.Dense(256, activation='relu')(x)
x = layers.Dense(128, activation='relu')(x)
x = layers.Dense(64, activation='relu')(x)
'''

# out1 = layers.Dense(1, activation='linear', name='NU_NOTA_MT')(x)
# out2 = layers.Dense(1, activation='linear', name='NU_NOTA_CN')(x)
# out3 = layers.Dense(1, activation='linear', name='NU_NOTA_CH')(x)
# out4 = layers.Dense(1, activation='linear', name='NU_NOTA_LC')(x)
# out5 = layers.Dense(1, activation='linear', name='NU_NOTA_REDACAO')(x)

out = layers.Dense(5, activation='linear')(x)

model = Model(inputs=inp, outputs=out)

from keras import optimizers
from keras import metrics

#rmsprop = optimizers.RMSprop(lr=0.0001)
nadam = optimizers.Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)



model.compile(optimizer=nadam, loss=['mse'], 
              metrics=['mae'])


# %%
history = model.fit(part_reader_f(partial_train),
                              steps_per_epoch=len(partial_train),
                              epochs=10,
                              validation_data=part_reader(validation),
                              validation_steps=len(validation))

model.save(os.path.join(script_dir, '../models/model2.h5'))
